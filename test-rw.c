#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define CHAR_DEV        "/dev/ecpu"
#define ECPU_SIZE       12
#define REG_SIZE        2
#define ID_OFFSET       0x00
#define DI_OFFSET       0x02
#define DO_OFFSET       0x04
#define DI_FIL_OFFSET   0x06
#define DO_FIL_OFFSET   0x08
#define STAT_OFFSET     0x0A

#define DI_FIL_VALID_BITS   0x802F
#define STAT_VALID_BITS     0x001F

int main(int argc, char *argv[]) {
    int fd;
    size_t size, processed;
    off_t offset;
    uint16_t rbuff[ECPU_SIZE/sizeof(uint16_t)], wbuff[ECPU_SIZE/sizeof(uint16_t)];

    // Test reading ID register
    fd = open(CHAR_DEV, O_RDONLY);
    if (fd < 0) {
        fprintf(stderr, "%s: [FAIL] Unable to open device %s.\n",
            argv[0], CHAR_DEV);
        return -1;
    }
    memset(rbuff, 0, ECPU_SIZE);
    offset = ID_OFFSET;
    size = REG_SIZE;
    processed = pread(fd, rbuff, size, offset);
    if (processed != size) {
        fprintf(stderr, "%s: [FAIL] Incorrect number of bytes read: %d.\n",
            argv[0], processed);
        return -1;
    }
    if ( (*rbuff & 0xFF00) != 0x0600 ) {
        fprintf(stderr, "%s: [FAIL] Incorrect ID register read: 0x%04x.\n",
            argv[0], *rbuff);
        return -1;
    }
    printf("%s: [PASS] Read ID Register\n", argv[0]);
    close(fd);

    // DO-DI loopback test
    fd = open(CHAR_DEV, O_RDWR);
    if (fd < 0) {
        fprintf(stderr, "%s: [FAIL] Unable to open device %s.\n",
            argv[0], CHAR_DEV);
        return -1;
    }
    memset(wbuff, 0, ECPU_SIZE);
    memset(rbuff, 0, ECPU_SIZE);
    offset = DO_OFFSET;
    size = REG_SIZE;
    *wbuff = 0xC5A3;
    processed = pwrite(fd, wbuff, size, offset);
    if (processed != size) {
        fprintf(stderr, "%s: [FAIL] Incorrect number of bytes written: %d.\n",
            argv[0], processed);
        return -1;
    }
    sleep(1);
    offset = DI_OFFSET;
    processed = pread(fd, rbuff, size, offset);
    if (processed != size) {
        fprintf(stderr, "%s: [FAIL] Incorrect number of bytes read: %d.\n",
            argv[0], processed);
        return -1;
    }
    if ( *rbuff != *wbuff ) {
        fprintf(stderr, "%s: [FAIL] DI value (0x%04x) does not match DO value (0x%04x).\n",
            argv[0], *rbuff, *wbuff);
        return -1;
    }
    printf("%s: [PASS] DO-DI Loopback\n", argv[0]);
    close(fd);

    // Readback DO register
    fd = open(CHAR_DEV, O_RDWR);
    if (fd < 0) {
        fprintf(stderr, "%s: [FAIL] Unable to open device %s.\n",
            argv[0], CHAR_DEV);
        return -1;
    }
    memset(wbuff, 0, ECPU_SIZE);
    memset(rbuff, 0, ECPU_SIZE);
    offset = DO_OFFSET;
    size = REG_SIZE;
    *wbuff = 0xC5A3;
    processed = pwrite(fd, wbuff, size, offset);
    if (processed != size) {
        fprintf(stderr, "%s: [FAIL] Incorrect number of bytes written: %d.\n",
            argv[0], processed);
        return -1;
    }
    processed = pread(fd, rbuff, size, offset);
    if (processed != size) {
        fprintf(stderr, "%s: [FAIL] Incorrect number of bytes read: %d.\n",
            argv[0], processed);
        return -1;
    }
    if ( *rbuff != *wbuff ) {
        fprintf(stderr, "%s: [FAIL] Read value (0x%04x) does not match written value (0x%04x) in DO.\n",
            argv[0], *rbuff, *wbuff);
        return -1;
    }
    printf("%s: [PASS] DO Read Back\n", argv[0]);
    close(fd);

    // Readback DI Filter register
    fd = open(CHAR_DEV, O_RDWR);
    if (fd < 0) {
        fprintf(stderr, "%s: [FAIL] Unable to open device %s.\n",
            argv[0], CHAR_DEV);
        return -1;
    }
    memset(wbuff, 0, ECPU_SIZE);
    memset(rbuff, 0, ECPU_SIZE);
    offset = DI_FIL_OFFSET;
    size = REG_SIZE;
    *wbuff = DI_FIL_VALID_BITS;
    processed = pwrite(fd, wbuff, size, offset);
    if (processed != size) {
        fprintf(stderr, "%s: [FAIL] Incorrect number of bytes written: %d.\n",
            argv[0], processed);
        return -1;
    }
    processed = pread(fd, rbuff, size, offset);
    if (processed != size) {
        fprintf(stderr, "%s: [FAIL] Incorrect number of bytes read: %d.\n",
            argv[0], processed);
        return -1;
    }
    if ( *rbuff != *wbuff ) {
        fprintf(stderr, "%s: [FAIL] Read value (0x%04x) does not match written value (0x%04x) in DI Filter.\n",
            argv[0], *rbuff, *wbuff);
        return -1;
    }
    printf("%s: [PASS] DI Filter Read Back\n", argv[0]);
    close(fd);

    // Readback DO Filter register
    fd = open(CHAR_DEV, O_RDWR);
    if (fd < 0) {
        fprintf(stderr, "%s: [FAIL] Unable to open device %s.\n",
            argv[0], CHAR_DEV);
        return -1;
    }
    memset(wbuff, 0, ECPU_SIZE);
    memset(rbuff, 0, ECPU_SIZE);
    offset = DO_FIL_OFFSET;
    size = REG_SIZE;
    *wbuff = 0xC5A3;
    processed = pwrite(fd, wbuff, size, offset);
    if (processed != size) {
        fprintf(stderr, "%s: [FAIL] Incorrect number of bytes written: %d.\n",
            argv[0], processed);
        return -1;
    }
    processed = pread(fd, rbuff, size, offset);
    if (processed != size) {
        fprintf(stderr, "%s: [FAIL] Incorrect number of bytes read: %d.\n",
            argv[0], processed);
        return -1;
    }
    if ( *rbuff != *wbuff ) {
        fprintf(stderr, "%s: [FAIL] Read value (0x%04x) does not match written value (0x%04x) in DO Filter.\n",
            argv[0], *rbuff, *wbuff);
        return -1;
    }
    printf("%s: [PASS] DO Filter Read Back\n", argv[0]);
    close(fd);

    // Readback Status register
    fd = open(CHAR_DEV, O_RDWR);
    if (fd < 0) {
        fprintf(stderr, "%s: [FAIL] Unable to open device %s.\n",
            argv[0], CHAR_DEV);
        return -1;
    }
    memset(wbuff, 0, ECPU_SIZE);
    memset(rbuff, 0, ECPU_SIZE);
    offset = STAT_OFFSET;
    size = REG_SIZE;
    *wbuff = STAT_VALID_BITS;
    processed = pwrite(fd, wbuff, size, offset);
    if (processed != size) {
        fprintf(stderr, "%s: [FAIL] Incorrect number of bytes written: %d.\n",
            argv[0], processed);
        return -1;
    }
    processed = pread(fd, rbuff, size, offset);
    if (processed != size) {
        fprintf(stderr, "%s: [FAIL] Incorrect number of bytes read: %d.\n",
            argv[0], processed);
        return -1;
    }
    if ( *rbuff != *wbuff ) {
        fprintf(stderr, "%s: [FAIL] Read value (0x%04x) does not match written value (0x%04x) in Status.\n",
            argv[0], *rbuff, *wbuff);
        return -1;
    }
    printf("%s: [PASS] Status Read Back\n", argv[0]);
    close(fd);

    return 0;
}
