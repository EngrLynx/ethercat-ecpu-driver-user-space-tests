## How To Use:

1. Download this repo by running `git clone git@bitbucket.org:EngrLynx/ethercat-ecpu-driver-user-space-tests.git` while in the `src` folder of the [Ethercat SDK Container](https://bitbucket.org/EngrLynx/ethercat-sdk-container).
2. Rename `ethercat-ecpu-driver-user-space-tests` to `tests`.
3. Move `make-tests.sh` to the `src` folder.
