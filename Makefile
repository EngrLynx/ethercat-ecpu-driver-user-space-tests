TARGET := test-rw

OBJFILES := $(patsubst %.c,%.o,$(wildcard *.c)) $(patsubst %.c,%.o,$(wildcard src/*.c))

GCC := $(CROSS_COMPILE)gcc

ARCHFLAGS := -march=$(ARCH)

DEBUGFLAGS := -DDEBUG

CCFLAGS := -g -Wfatal-errors -fmax-errors=5

all: $(OBJFILES)
	$(GCC) -o $(TARGET) $(OBJFILES) $(ARCHFLAGS) $(DEBUGFLAGS) $(CCFLAGS) $(INCLUDES) $(LDFLAGS)

install:
	cp $(TARGET) $(DESTDIR)

clean:
	rm -rf *.o *~ *.tmp *.log $(TARGET)
